import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  number: 0
};

// outside of this slice: state.counter.number
export const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    // inside the slice; counter.number
    increment: counter => {
      counter.number += 1;
    },
    decrement: counter => {
      counter.number -= 1;
    },
    // actions: { type: 'increaseBy', payload: value }
    increaseBy: (counter, action) => {
      console.log(action);
      counter.number += action.payload;
    },
  }
});

export const { increment, decrement, increaseBy } = counterSlice.actions;
export default counterSlice.reducer;
