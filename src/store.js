import { configureStore } from "@reduxjs/toolkit";
import counterReducer from './counterSlice';
// const counterReducer = counterSlice.default

export const store = configureStore({
  reducer: {
    counter: counterReducer,
    // likes: likesReducer,
    // matches: matchesReducer,
    // currentUser: userReducer,
  },
});
