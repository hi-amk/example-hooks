import { useState } from 'react';
import './App.css';
import { increment, decrement, increaseBy } from './counterSlice';
import { useSelector, useDispatch } from 'react-redux';

// useSelector -> allows us to get values from the state inside the store
// useDispatch -> allows us to send actions (commands) to the store

// class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       number: 0,
//       input: 0,
//     };
//   }

//   handleIncreaseClick() {
//     this.setState({ number: this.state.number + 1 });
//   }
// }

function App() {
  // const [number, setNumber] = useState(0);
  const [localIncreaseBy, setLocalIncreaseBy] = useState(7);
  // const [state, setState] = useState({ number: 0, input: 0 });

  function handleSetIncreaseBy(e) {
    const value = e.target.value;
    const valueNum = Number.parseInt(value);
    setLocalIncreaseBy(valueNum);
  }
  const number = useSelector(state => state.counter.number);
  const dispatch = useDispatch();

  return (
    <div className="App">
      <input type="number" onChange={e => handleSetIncreaseBy(e)} value={localIncreaseBy} />
      <button onClick={() => dispatch(increaseBy(localIncreaseBy))}>Increase By</button>
      <button onClick={() => dispatch(decrement())}>Decrease</button>
      <span>{ number }</span>
      <button onClick={() => dispatch(increment())}>Increase</button>
    </div>
  );
}

export default App;
